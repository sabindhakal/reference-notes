
# Gpg Key

The command to generate a new key is:

    gpg --full-gen-key

Please ensure you use at least 4096 RSA for the new key
Then share the public part of your new key with the team with the command:

    gpg -a --export newcomer@mytoys.de > newcomer.pub

With that public key another member of the team will encrypt the backend@team for you with the commands:

    gpg -a --export backend@mytoys > backend.key gpg -a --export-secret-keys backend@mytoys >> backend.key gpg -a -e -u newcomer@mytoys.de -r newcomer@mytoys.de backend.key

And send you the file backend.key.asc via Slack or email for you to decrypted and imported with the commands:

    gpg -d backend.key.asc > backend.key gpg --import backend.key

Check you have the public part of the key with the command gpg -k backend@mytoys.de and also the secret part with the command gpg -K backend@mytoys.de (please notice the lower/upper case k for pub/secret keys info)

IMPORTANT: The GPG secret keys should NOT be stored outside your computer. Only the public parts are shared (and only with trusted parties)
