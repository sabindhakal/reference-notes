# netstat

Display the active connections, routing tables, network interfaces and protocol statistics.

       netstat [-vWnNcaeol] [<Socket> ...]
       netstat { [-vWeenNac] -i | [-cnNe] -M | -s [-6tuw] }

        -r, --route              display routing table
        -i, --interfaces         display interface table
        -g, --groups             display multicast group memberships
        -s, --statistics         display networking statistics (like SNMP)
        -M, --masquerade         display masqueraded connections

        -v, --verbose            be verbose
        -W, --wide               don't truncate IP addresses
        -n, --numeric            don't resolve names
        --numeric-hosts          don't resolve host names
        --numeric-ports          don't resolve port names
        --numeric-users          don't resolve user names
        -N, --symbolic           resolve hardware names
        -e, --extend             display other/more information
        -p, --programs           display PID/Program name for sockets
        -o, --timers             display timers
        -c, --continuous         continuous listing

        -l, --listening          display listening server sockets
        -a, --all                display all sockets (default: connected)
        -F, --fib                display Forwarding Information Base (default)
        -C, --cache              display routing cache instead of FIB
        -Z, --context            display SELinux security context for sockets


Display all network interface

    netstat -ie

Display current routing table

    netstat -r

Display continuous output information about current active connection (tcp/udp)

    netstat -c

Display all current connection 

    netstat -a 
    netstat -at // all tcp connections
    netstat -au // all upd connections

Display all listening ports

    netstat -lt // listening tcp ports
    netstat -lu // listening udp ports

Process identification

    netstat -atp

Show only listening connections

    netstat -tnl // tcp
    netstat -unl // upp

Looking for specific port or service

    netstat -nlp | grep :22

    netstat -an | grep :22
