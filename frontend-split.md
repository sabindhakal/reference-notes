# Deploying a component


Step 0: A deployment is triggered with a desired branch to be deployed e.g. release/20200201. 

Step 1: The jenkins jobs triggers another job which is Assets Builder. Asset Builder does the following steps to build the assets.
Clone the component repository (e.g.: msv_ui_rating_service)
Clone the ms_frotend repository
Copy the assets from rating repository to ms_frontend repository (which contains build task)
Build all the production / dev assets
Upload the new assets to S3 (or wherever it can be accessed by another job)



# Versioning with a table 

A table(csv, json...) as  a file with all the version of current components is maintained and written by the Assets Builder. 

* This file is taken into account for each deployment
* Possibly stored in S3 or DynamoDB table

Possible format:
    {
        component-name: "msv_ui_rating_service",
        live-version: "release/20200203",
        previous-live-version: "release/20200201",
        vsc-link: "https//mytoys.gitlab/...."
    },
    {
        component-name: "ms_frontend",
        live-version: "release/20200201",
        previous-live-version: "release/20200101",
        vsc-link: "https//mytoys.gitlab/...."
    },
    {
        component-name: "msv_ui_header_service",
        live-version: "release/20200201",
        previous-live-version: "release/20200101",
        vsc-link: "https//mytoys.gitlab/...."
    }

Asset builder always refers to this file for cloning all the repositories.  After a component is successfully deployed this file is always updated.

# Step 2

Jenkins job to deploy a specific component. This job triggers two parallel jenkins job.

## 2.1 Create rating deployment

This job is responsible for deploying a new version of rating-service to the aws. 

Acceptance criteria:

* Clone rating_service (release branch is provided from the main jenkins step 0.)
* Run composer install with prod-assets
* Zip the artifact with .ebextension
* Upload the artifact to S3 and tag them as new deployment

Note: Assets building like npm/yarn is not required

For reference pipeline script see ms_frontend cloud deployment pipeline 

## 2.2 Create ms_frontend deployment with assets

This job is responsible for deploying a new version of ms_frontend to aws.


Acceptance criteria:

* Download the versioning table
* Clone the ms_frontend repo (with the live-version branch from table)
* Copy built assets from Step 1 to the respective directories
* Composer install (prod optimized)
* Create an artifact with all required files
* Upload file to S3
* Tagged the uploaded file as new Deployment

Note: Assets building like npm/yarn is not required

For reference pipeline script see ms_frontend cloud deployment pipeline.
