# Terraform cheat sheet


### Variables & data types

```
# string
variable "aws_region" {
    type = string
    default = "eu-central-1"
} 

# map
variable "mymap" {
    type = map(string)
    default = {
        mykey = "my value"
    } 
}

# list
variable "mylist" {
    type = list
    default = [1,2,3] 
}
```

### Using output files

Useful when long outputs

```
terraform plan -out out.terraform
terraform apply out.terraform

# shotcut
terraform -out file; terraform apply file
```

### Terraform commands

| Command            | Action                                                                  | 
| ------------------ |:-----------------------------------------------------------------------:|
| terraform fmt      | Format code                                                             |
| terraform grap     | Create visual grah                                                      |
| terraform get      | Download and update modules                                             |
| terraform output   | Output any output any of your resources                                 |
| terraform refresh  | Refresh remote states                                                   |
| terraform show     | Show human readable outp                                                |
| terraform state    | Used for advanced cases like renaming resources                         |


### Using terraform console

Terraform console allows you to quickly check the values of variables that is defined within your terraform file. 

```
terraform console
var.aws_region # gets the value of variable aws_region
```

    

### Useful links

https://github.com/wardviaene/terraform-course
https://github.com/wardviaene/devops-box
