# How computers work

* Computers run on electricity
* Electricity can be ON or OFF
* Coding Scheme
  * ON or OFF state have meaning associated to them
    * 1 bulb can represent 2 messages (e.g.)
      * ON (1) = go
      * OFF (0) = stop
    * 2 bulbs can have 2^2 = (4) messages
      * 0 0 = red
      * 0 1 = green
      * 1 0 = blue
      * 1 1 = pink
    * 3 bulbs can have 2^3 = (8) messages
    * 4 bulbs can have 2^4 = (16) messages
    * 5 bulbs can have 2^5 = (32) messages
    * 6 bulbs can have 2^6 = (64) messages
    * 7 bulbs can have 2^7 = (128) messages
    * 8 bulbs can have 2^8 = (256) messages

> ## 2<sup>n</sup>

## Numeral systems


