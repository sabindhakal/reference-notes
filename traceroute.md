# Traceroute

It is a tool that allows you to see how packets are being transmitted from source to the destination.
It helps you see which path the packets take. Traceroute uses the TTL(Time To Live) field in IP header.

TTL filed is normally used to prevent routing loop. Example because of some routing misconfiguration a packet might end up in an infinite loop.

Every time a router (hop) transmits a packet to another router it decrements the TTL by 1. When the TTL reaches 0, the packet is dropped. When the router drops the packet because TTL is 0, it sends message back to the source saying that the packet has been dropped.

Traceroute exploits the TTL field to identify the routers along the path that a packets takes from a source to a destination.

* Common TTL value is 64.

## How does traceroute work?

![Demo Image](resources/traceroute.png)

Traceroute uses ICMP protocol.

First of all the traceroute sends a packet with TTL with value 1. The first node receiving the packets decrements the TTL to 0 and drops the packet. Whe the packet is dropped the node sends back info to the Sender that packet has been dropped.

On the next iteration TTL is set to 2. Thus the packets travels two hops. The second hop sends back the info. regarding the packet has been dropped.

These steps continue, till the packet reaches its destination. Doing this it tries to get all the information about the router along the path.

### NOTE:
* Not all routers are configured to send back the icmp responses
* Network has to allow those responses to com back
