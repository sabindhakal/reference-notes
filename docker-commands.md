# Docker commands

##### Mysql

    docker run --name ajb-mysql -e MYSQL_ROOT_PASSWORD=root -d -p 3306:3306 mysql:5.7

##### Crate bridge network

    docker network create local-dev-network

##### Connect existing ajb-mysql to local-dev-network

    docker network connect local-dev-network ajb-mysql

##### Rating Service

    docker build -t "msv_ui_rating_service_image" .
    docker run --name msv-ui-rating-service --network local-dev-network -v $(pwd)/php.ini-develop:/usr/local/etc/php/php.ini  -p 8088:80 -d -v/Users/sdhakal/workspace/msv_ui_rating_service:/var/www/html msv_ui_rating_service_image

##### Frontend

    docker build -t "ms-frontend-image" .
    docker run --name ms_frontend -v $(pwd)/php.ini-develop:/usr/local/etc/php/php.ini  -p 80:80 -d -v/Users/sdhakal/workspace/ms_frontend:/var/www/html -v /tmp/xhprof:/tmp/xhprof ms-frontend-image

##### With rating conainer

    docker run --name ms-frontend --network local-dev-network -v $(pwd)/php.ini-develop:/usr/local/etc/php/php.ini  -p 80:80 -d -v/Users/sdhakal/workspace/ms_frontend:/var/www/html -v /tmp/xhprof:/tmp/xhprof ms-frontend-image

##### redis

    docker run --name ms-redis --network local-dev-network redis

##### postgres

    docker run --name ajb-postgres -e POSTGRES_PASSWORD=root -d -p 5432:5432 postgres

##### mongodb

    docker run --name ajb-docker -d -p 27017:27017 -v ~/mongodata/:/data/db mongo

##### sample symfony-4 apache docker
    FROM php:7.2-apache

    WORKDIR /var/www/html
    COPY rating-apache.conf /etc/apache2/sites-enabled/rating-apache.conf
    COPY php.ini-develop /usr/local/etc/php/php.ini

    RUN rm /etc/apache2/sites-enabled/000-default.conf

    RUN apt-get update && apt-get install -y curl libicu-dev git graphviz

    RUN yes | pecl install xdebug
    RUN docker-php-ext-enable xdebug

    RUN docker-php-ext-configure intl
    RUN docker-php-ext-install intl
    RUN docker-php-ext-enable intl

    RUN docker-php-ext-install mysqli pdo pdo_mysql

    RUN a2enmod headers rewrite

##### elastic search
    docker run -p 9200:9200 -d --name ajb-elasticsearch -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.4.2
