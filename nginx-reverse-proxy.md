
# Using nginx as reverse proxy


    docker network create dev-nw

    docker run -u 0 --restart=always -d -v ~/management_home/jenkins_home:/var/jenkins_home -p 8080:8080 -p 50000:50000 --name ajb-jenkins --network dev-nw jenkins/jenkins:lts
    docker run -u 0 --restart=always -d -p 1880:1880 -v ~/management_home/node_red:/data --name ajb-nodered --network dev-nw nodered/node-red
    docker run -u 0 --restart=always -d -p 80:80 -v ~/management_home/nginx/default.conf/default.conf:/etc/nginx/conf.d/default.conf:ro --name ajb-nginx-proxy
     --network dev-nw nginx:stable-alpine
     docker run -u 0 --restart=always -d -p 80:80 -v ~/management_home/nginx/conf.d/default.conf:/etc/nginx/conf.d/default.conf:ro --name ajb-nginx-proxy --network dev-nw nginx:stable-alpine 
    

### Sample configuration

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;
        #access_log  /var/log/nginx/host.access.log  main;

        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
            proxy_pass http://ajb-jenkins:8080/;
        }

        location /nodered {
            rewrite /nodered/(.*) /$1 break;
            proxy_pass http://ajb-nodered:1880;

            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_http_version 1.1;
            proxy_intercept_errors  on;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }
