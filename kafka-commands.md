# Apache kafka

## Installation

* Install brew if needed: /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
* Download and Setup Java 8 JDK:
* brew tap caskroom/versions
* brew cask install java8
* Download & Extract the Kafka binaries from https://kafka.apache.org/downloads
* Install Kafka commands using brew: brew install kafka
* Try Kafka commands using kafka-topics (for example)
* Edit Zookeeper & Kafka configs using a text editor
    zookeeper.properties: dataDir=/your/path/to/data/zookeeper
    server.properties: log.dirs=/your/path/to/data/kafka
* Start Zookeeper in one terminal window: 
    zookeeper-server-start config/zookeeper.properties
* Start Kafka in another terminal window: 
    kafka-server-start config/server.properties

## Commands

#### Start zookeeper
    cd ~/kafka-dir
    zookeeper-server-start config/zookeeper.properties

#### Start kafka
    kafka-server-start config/server.properties

#### Create Topic
    kafka-topics --create --zookeeper 127.0.0.1:2181 --topic firsttopic --create --partitions 3 --replication-factor 1

####  List all topics 
    kafka-topics --zookeeper 127.0.0.1:2181 --list

#### Describ a topic
    kafka-topics --zookeeper 127.0.0.1:2181 --topic firsttopic --describe

#### Delete a topic
    kafka-topics --zookeeper 127.0.0.1:2181 --topic secondtopic --delete

#### Start a producer
    kafka-console-producer --broker-list 127.0.0.1:9092 --topic first_topic

Specifying property so the acs=all, (the ack is received only after message is persisted in all broker)
kafka-console-producer --broker-list 127.0.0.1:9092 --topic first_topic --producer-property acks=all

Create topic from a producer directly --- (not recommended, no of partitions=1 and replication=1 as default) // these default values can be modified in servers.properties
kafka-console-producer --broker-list 127.0.0.1:9092 --topic new_topic


#### Start consumer
     kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic firsttopic

#### Start conumer & list all topics from beginning
    kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic firsttopic --from-beginning

#### -- DEMO --
start a consumer with topic first topic

    kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic firsttopic --group my-first-app

start a second consumer with topic firsttopic in a diffetent terminal
    
    kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic firsttopic --group my-first-app

start a producer in a diffetent terminal
    
    kafka-console-producer --broker-list 127.0.0.1:9092 --topic firsttopic
    >red
    >green
    >blue

![Demo Image](resources/demo-img.png)


## Kafka PHP

### Insall rdkafka
    
    $ git clone https://github.com/edenhill/librdkafka.git
    $ cd librdkafka
    $ ./configure
    $ make && make install
Edit php.ini file and add

    //[kafka]
    //extension="/Applications/MAMP/bin/php/php7.2.8/lib/php/extensions/no-debug-non-zts-20170718/rdkafka.so"

