# Useful commands

##### curl dowoload a file

###### Download file

    curl http://example.com --output my.file

###### GET / POST / PUT / DELETE

    curl -X PUT -H "Content-Type: Application/json"  -d '{"course": "Kafka", "module": "Elastic search"}' localhost:9200/twitter/tweets/1

##### Curl mesasure response times

Create a new file, curl-format.txt, and paste in

    time_namelookup:  %{time_namelookup}\n
    time_connect:  %{time_connect}\n
    time_appconnect:  %{time_appconnect}\n
    time_pretransfer:  %{time_pretransfer}\n
    time_redirect:  %{time_redirect}\n
    time_starttransfer:  %{time_starttransfer}\n
    ----------\n
    time_total:  %{time_total}\n

Make a request

    curl -w "@curl-format.txt" -o /dev/null -s "http://wordpress.com/"

Source: https://stackoverflow.com/questions/18215389/how-do-i-measure-request-and-response-times-at-once-using-curl

##### Curl only show header

    curl -I /

##### Curl check if gzip is enabled

    curl -I -H "Accept-Encoding: gzip" 'https://namastejob.com' 2>/dev/null | grep -i 'Content-Encoding: gzip'

##### extract .tgz

    tar -xvzf /path/to/yourfile.tgz
    x - extract
    v - verbose
    z - gunzip
    f - file, should be last just before filename
    -C - to extract to a different dir. tar -xvzf .file.tgz -c /my/path

##### move file and rename

    mv file-src/* file-dest/

#### creating ssh tunnel (ssh port forwarding)

    // local forwarding
    ssh -L 80:intra.example.com:80 gw.example.com
    // This example opens a connection to the gw.example.com jump server, 
    // and forwards any connection to port 80 on the local machine to port 80 on intra.example.com.

    ssh -L 9121:stable-product-service.ber.mytoys.de:9121 -l sdhakal sshgw.ber.mytoys.de

    ssh -L 8181:mytoys.sabin.dev-frontend.ber.mytoys.de:80 -l sdhakal sshgw.ber.mytoys.de
    // forward remote 80 to local 8181

    // remote forwarding
    ssh -R 8080:localhost:80 public.example.com
    // This allows anyone on the remote server to connect to TCP port 8080 on the remote server. 
    // The connection will then be tunneled back to the client host, and the client then makes a TCP connection to port 80 on localhost. 
    // Any other host name or IP address could be used instead of localhost to specify the host to connect to.

NOTE: If you are not forwarding to localhost then you'll need to add an entry on /etc/hosts file
    /etc/hosts
    127.0.0.1 mytoys.sabin.dev-frontend.ber.mytoys.de

