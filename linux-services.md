### Start / Stop service

`service httpd start`

or

`systemctl start httpd`

`systemctl stop httpd`

### Check service status

`systemctl status httpd`

### Enable a service on system-startup


`systemctl enable httpd`

### Disable a service on system-startup

`systemctl disable httpd`

## Creating a scrit as service

Example script .. which we want to creat a service

`/usr/bin/php /var/www/symfony/bin/console server:run`

This aboce script can be created to a service by creating a systemd service, these config file are located in

`/etc/systemd/system`

Create  `my_app.service` file with following contecnt

```
[Service]
ExecStart=/usr/bin/php /var/www/symfony/bin/console server:run
```
