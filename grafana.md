# Grafana

### Delete annotations

    curl -X DELETE -H "Authorization: Bearer eyJrIjoiTDRlQXl2VFh4UjJNdXVPVThWaE5weTZmMm5IaDIzNjQiLCJuIjoiYW5ub3RhdGlvbl9hZG1pbiIsImlkIjoxfQ==" -H "Content-Type: application/json" https://localhost:3000/api/annotations/10

### Create annotations

    curl -X POST -H "Authorization: Bearer lkIjoxfQ==" -H "Content-Type: application/json" -d '{"text": "TEST New Deployment", "tags": ["deployment", "release:test-01"]}' https://grafana.frontends-prod.aws.mytoys.de/api/annotations
