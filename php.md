# PHP basic command

### Disable memory limit

    php -d memory_limit=-1 composer update

### Run symfony web-sever in port 80

    /Applications/MAMP/bin/php/php7.3.8/bin/php bin/console server:run 0.0.0.0:80 
