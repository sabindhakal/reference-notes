# GoLang Ref.

## Presentations

https://goo.gl/Tbz6Xf

https://goo.gl/PHKgO7

## GITHUB CODE

http://goo.gl/KbUroF

https://github.com/GoesToEleven/GolangTraining

## Helpful links

https://gobyexample.com/

https://godoc.org

https://golang.org


### Go workspace

* one folder - any name, any location
    * bin
    * pkg
    * src
      * github.com
        * <github.com username>
          * folder with code for project /repo
          * folder with code for project /repo
          * folder with code for project /repo


### Go get
Get the package so that it can be used

Example

    go get uuid

### Env. Variables

* GOPATH
  * points to your go workspace
* GOROOT
  * points to your binary installation of go


### Commands

#### go run

    go run main.go

#### go build

* for an executable:
  * builds the file
  * reports errors, if any
  * if no errors, puts an executable in the current folder
* for a package
  * builds the file
  * reports error, if any
  * throws away binary

#### go install

* for an executable:
  * compiles the program
  * names the executable the folder anem holding the code
  * puts teh executable in workspace /bin
    * $GOPATH / pkg
  * makes it an archive file


### Package management

https://research.swtch.com/deps

#### Creating a module
    * go mod init creates a new module, initializing go.mod file that describes it.
    * go build, go test, and other package-building commands add new dependencies to go.mod
    * go list -m  all prints the current module's dependencies.
    * go get changes the required version of teh dependency (or adds a new dependency)
    * go mod tidy removes unused dependencies

### Creating user-defined type

    package main

    import (
        "fmt"
    )

    var a int // a is of type int
    type hotdog int // define a new type with underlying type int
    var b hotdog // b is of type hotdog

    func main() {
        a = 42
        b = 43
        fmt.Println(a)
        fmt.Printf("%T\n", a) // prints the type of a -> int

        fmt.Println(b)
        fmt.Printf("%T\n", b) // prints the ype of b -> main.hotdog

    }

### Converting a type to another (casting)

    package main

    import (
        "fmt"
    )

    var a int // a is of type int
    type hotdog int // define a new type with underlying type int
    var b hotdog // b is of type hotdog

    func main() {
        a = 42
        b = 43
        fmt.Println(a)
        fmt.Printf("%T\n", a) // prints the type of a -> int

        fmt.Println(b)
        fmt.Printf("%T\n", b) // prints the ype of b -> main.hotdog

        a = int(b) // convert hotdog to int and assign the value to a
        fmt.Println(a)
        fmt.Printf("%T\n", a) // prints the type of a -> int

    }

## Numeric types

https://golang.org/ref/spec#Numeric_types

## String types

Can be created using "" or \`\`. If created with \`\` it can be used in multiple lines.

    s := "Hello World"
    s1 := `"Hello
            World"`

A string type represents the set of string values. A string type is a(possibly empty) sequence of bytes. Strings are immutable: once created, it is impossible to change the contents of the string. 

sequence of byte

byte is an alisa for uint8.

slice(array) of bytes.

    package main
    import (
        "fmt"
    )
    // convert type string to type byte
    func main() {
        s := "hello playground"
        fmt.Println(s)
        fmt.Printf("%T\n", s) // string

        bs := []byte(s)
        fmt.Println(bs)
        fmt.Printf("%T\n", bs) // unit8
    }

OUTPUT

    hello playground
    string
    [104 101 108 108 111 32 112 108 97 121 103 114 111 117 110 100]
    []uint8


[104 101 108 108 111 32 112 108 97 121 103 114 111 117 110 100] -> https://en.wikipedia.org/wiki/ASCII

    104 - h
    101 - e
    108 - l
    108 - l
    111 - o
 
## Control flow

### loops

    i := 1
    for i <= 3 {
        fmt.Println(i)
        i = i +1
    }

    for j :=7; j <= 9; j++ {
        fmt.Println(j)
    }

    for {
        fmt.Println("loop")
        break
    }

    for n :=0; n <=5; n++ {
        if n%2 == 0 {
            continue
        }
        fmt.Println(n)
    }

Printing ascii
https://golang.org/pkg/fmt/

    for i :=33; i <= 122; i++ {
		fmt.Printf("%v\t\t%#U\t\t%#x\t\t%#b\n", i, i, i, i)
	}
    // value, ascii, hex, binary

## Data structures

## Array

An array is building block in Go.

Create an array

    var x [5]int
    // var x is an array of type int of size 5

    fun main() {
        var x [5]int
        fmt.Println(x)

        x[3] = 42 // assign value 42 at position 3
        fmt.Println(x)

        fmt.Println(len(x)) // len() gets the lenght of array

    }

### len()

gets the length of an array

    len(x)

> Arrays are not recommend in Go instead use slices


## Slices - composite literal

A composite data type is a type which is constructed using other primitive data-types and other composite types.

* Slice allows you to group together VALUES of the same TYPE
* Slices are defined in the same way as array, but you don't have to specify the lenght 

    func main() {
        // x := type{values} // composite literal
        x := []int{5,6,8,43}
        fmt.Println(x)
        // a SLICE allows you to group together VALUES of the same TYPE
    }


### Looping over a slice

    func main()  {
        x := []int{4, 6,78, 8, 2}
        fmt.Println(len(x))
        fmt.Println(x)
        fmt.Println(x[3])

        for i,v := range x {
            fmt.Println(i, v)
            // fmt.Printf("%v	%v\n", i, v)
        }
    }

### Slicing a slice

    func main()  {
        x := []int{4, 6,78, 8, 2}
        fmt.Println(x)
        fmt.Println(x[:]) // this syntax can be used to convert array to slice
        fmt.Println(x[1:]) // return from pos 1 to ..n
        fmt.Println(x[1:3]) // from pos 1 to 3(up to 3rd pos but doesn't include 34 pos
    }
 
 Output

    [4 6 78 8 2]
    [4 6 78 8 2]
    [6 78 8 2]
    [6 78]

### Append to a slice

Syntax

    func append(slice []T, elements ...T) []T

> ...T is variadic parameter, meaning that any no. of parameter of the Type T can be passed in here.

> y... here the ... operator is used to unfold the content of slice

    func appendTrial() {
        x := []string {"ram", "shyam", "hari", "git", "sita", "nita"}
        fmt.Println(x)
        
        // append values
        x = append(x, "pawan", "kumar")
        fmt.Println(x)

        // append another slice
        y := []string{"raju", "kaju", "thule"}
        x = append(x ,y...) // y... unfolds the slice
        fmt.Println(x)
    }

### Deleting from a slice

    func deleteFromSlice()  {
        x := []string {"ram", "shyam", "hari", "git", "sita", "nita"}
        fmt.Println(x)

        x = append(x[:2], x[3:]...) // [:2] take two elems "ram", "shyam" [3:] start from 3rd elem "gita","sita", "nita"
        fmt.Println(x)
    }

### Slice make() function

Slices are built on top-of array, if your slice grows all the values have to be copied to new array and old one is thrown away (which will take some processing power)

If you already have some estimate of how big your slice, you can use make() function to create the underlying array of big enough size. This will save the extra time and effort that compiler goes through in the run-time to change the size of underlying array. 

Syntax

    make([]T, length, capacity)
    // e.g
    make([]int, 20, 100)

Example 

    func makeExample() {
        x := make([]int, 5, 7) // length is 5 and 10 is the capacity
        fmt.Println(x)
        fmt.Println(len(x))
        fmt.Println(cap(x))

        x[0] = 32 // works
        x[4] = 20 // works
        // x[7] = 20 // doesn't work because we have specified lenght of 5
        x = append(x , 23) // works
        x = append(x , 18) // works
        fmt.Println(x)
        fmt.Println(len(x))
        fmt.Println(cap(x)) // here we reach the cap 7

        // the compiler than doubles the cap to the double  of previous value so cpa -> 17 now
        // this takes some processing power
        x = append(x , 9) // works
        x = append(x , 9) // works
        fmt.Println(x)
        fmt.Println(len(x))
        fmt.Println(cap(x))
    }

### Multi dimensional slice

Example: 

    func multiDimensionalSlice() {
        jb := []string{"James", "Bond", "Chocolate", "Martini"}
        fmt.Println(jb)
        mp := []string{"Money", "Penny", "Strawberry", "Bubblegum"}
        fmt.Println(mp)

        // now we have two slices
        xp := [][]string{jb, mp} //slice([])  of slice of string ([]string)
        fmt.Println(xp)
    }

Output:

    [James Bond Chocolate Martini]
    [Money Penny Strawberry Bubblegum]
    [[James Bond Chocolate Martini] [Money Penny Strawberry Bubblegum]]

## Maps

Maps are key value store. Allows fast and efficient lookup.  They are un-ordered list.

    func mapsTest() {
        // create map
        m := map[string]int{
            "James": 10,
            "Money": 20,
        }
        fmt.Println(m)

        // access by key
        fmt.Println(m["James"])

        // access the value and check if the key exists
        v, ok := m["Bond"]
        fmt.Println(v)
        fmt.Println(ok)

        // idiomatic chunk to check if val exits
        if v, ok := m["Money"]; ok {
            fmt.Println("THIS IS THE IF PRINT", v)
        }

        for k, v := range m {
            fmt.Println(k, v)
        }
    }

### Del from a map

    func delMap() {
        // create map
        m := map[string]int{
            "James": 10,
            "Money": 20,
        }
        fmt.Println(m)

        delete(m, "James")
        delete(m, "Ian Fleming") // it is possible to del sth. that doesn't exist
        fmt.Println(m)

        // check and delete
        if v, ok := m["Money"]; ok {
            fmt.Println("value: ", v)
            delete(m, "Money")
        }
        fmt.Println(m)
    }

## Struct

Struct is an aggregate data-type. Allows you to (compose together) aggregate values of different type.

    type person struct {
        first string
        last string
    }

    func main()  {
        p1 := person{
            first: "James",
            last: "Bond",
        }

        p2 := person{
            first: "Miss",
            last: "Moneypenny",
        }

        fmt.Println(p1)
        fmt.Println(p2)
        
        fmt.Println(p1.first, p1.last)
        fmt.Println(p2.first, p2.last)
    }

### Embedded struct

    type person struct {
        first string
        last string
    }

    type secretAgent struct {
        person
        liscenceToKill bool
    }

    func main()  {
        sa1 := secretAgent {
            person: person{
                first: "James",
                last: "Bond",
            },
            liscenceToKill: true,
        }

        fmt.Println(sa1)
        fmt.Println(sa1.first, sa1.last) // here the first, last from person got promoted to secretagent
        // should there be collision meaning secretagent also has property named first the upper one will be preferred
    }

### Anonymous struct

Struct without name (anonymous). There are use-cases where you don't want to crate extra variable e.g sending data from controller to view*

    func anonymousStructTest() {
        p1 := struct { // declared dynamically
            first string
            last string
            age int
        } {
            first: "james",
            last: "bond",
            age: 32,
        }
        fmt.Println(p1)
    }

## Functions

Signature

    // func (r receiver) identifier(parameters) (return(s)) { ... }

> Everything in Go is passed by VALUE

Examples

    func foo() {
        fmt.Println("hello from foo")
    }

    func bar(s string) {
        fmt.Println("hello,", s)
    }

### Functions with multiple returns

    func mouse(fn string, ln string) (string, bool) {
        a := fmt.Sprint(fn, " ", ln, `, says hello`)
        b := false
        return a, b
    }

### Variadic parameters

Unlimited parameters

    func vardicTest(x ...int) {
        fmt.Println(x)
        fmt.Printf("%T\n", x)
    }

### Defer

Defers the execution of a function until where ever it is called comes to an end.

A "defer" statements invokes a function whose execution is deferred to the moment the surrounding function returns, either because the surrounding function executed a return statement, reached the end of its function body, or because the corresponding goroutine is panicking.

    func main()  {

        defer printFoo()
        printBar()
    }

    func printFoo() {
	    fmt.Println("foo")
    }

    func printBar() {
        fmt.Println("bar")
    }

Output
    bar
    foo

Here adding defer to printFoo() forces to wait for its execution until the main function finishes everything it needs to finish. 


### Methods

Example

    type person struct {
        first string
        last string
    }

    type secretAgent struct {
        person
        ltk bool
    }

    // Add speak method to secret agent
    // attach the function speak to anybody with type secret agent
    // any value of type secret-agent has access to this function
    func (s secretAgent) speak() {
        fmt.Println("I am", s.first, s.last)
    }

    func main()  {
        sa1 := secretAgent {
            person: person{
                "james",
                "bond",
            },
            ltk: true,
        }
        sa1.speak()
    }

## Interfaces & polymorphism

Interfaces allow us to define behavior and also allow us to do polymorphism.

> go lang. specification
> keyword identifier type
> 
> E.g:
> 
> var x int
> 
> type secretAgent struct
> 
> type human sturct

> A value can of more than one type!

    package main

    import (
        "fmt"
    )

    type person struct {
        first string
        last string
    }

    type secretAgent struct {
        person
        ltk bool
    }

    func (s secretAgent) speak()  {
        fmt.Println("I am", s.first, s.last, "- the secretAgent speaking!")
    }

    func (p person) speak()  {
        fmt.Println("I am", p.first, p.last, "- the person speaking!")
    }

    func bar(h human)  {
        switch h.(type) {
        case person:
            fmt.Println("I was passed into barrr", h.(person).first)
        case secretAgent:
            fmt.Println("I was passed into barrr", h.(secretAgent).first)
        }
        fmt.Println("I was passed into bar", h)
    }

    // defining this interface says that 
    // any type that has this type speak 
    // is also of type human
    type human interface {
        speak()
    }

    func main()  {
        sa1 := secretAgent{
            person: person {
                first: "James",
                last: "Bond",
            },
            ltk: true,
        }
        // value sa1 has multiple types
        // it is of type secretAgent and
        // as it has speak method attached to it
        // is also of type human
        sa1.speak()
        sa1.person.speak()

        sa2 := secretAgent{
            person: person{
                first: "Iean",
                last: "Fleming",
            },
            ltk: true,
        }

        p1 := person {
            first: "Dr.",
            last: "Yes",
        }

        bar(sa1)
        bar(sa2) //here bar is taking in both secretAgent
        bar(p1) // and person as parameter because interface person has the speak() method
    }

    ## Anonymous func

    package main

    import (
        "fmt"
    )

    func main() {
        foo()

        // anonymous function 
        func(x int) {
            fmt.Println("Anonymous function ran!", x)
        }(42)
    }
    func foo() {
        fmt.Println("foo ran")
    }

## Func expression

> Function is a first class citizen in go.
> means that function are similar to int, string ..

    func main() {
        f := func() {
            fmt.Println("my first func expression")
        }

        g := func(x int) {
            fmt.Println("g func expression", x)
        }

        f()
        g(10)
    }

## Returning a func

    func main()  {
        // function returning string
        s1 := foo()
        fmt.Println(s1)

        // anonymous func
        x := func () int {
            return 2000
        }
        fmt.Printf("%T", x)

        // returning func
        z := barz()
        fmt.Printf("%T", z)

        i := z()
        fmt.Println(i)

    }

    func foo() string  {
        return "hello world"
    }

    func barz() func() int {
        return func() int {
            return 2000
        }
    }

## Callback function

    func main() {
        numbers := []int{1,5,3,2,4,7,9,8}
        fmt.Println("Total:", sum(numbers...))
        fmt.Println("Even sum:", evenSum(sum, numbers...)) // pass sum as func param
    }

    func sum(xi ...int) int {
        total := 0
        for _, v := range xi {
            total += v
        }
        return total
    }

    func evenSum(f func(xi ...int) int, vi ...int) int { // first param. is a func
        var en []int
        for _, v := range vi {
            if v % 2 == 0 {
                en = append(en, v)
            }
        }
        return f(en...)
    }

## Closure

Enclose a variable around, so that the scope of the variable is limited to one area of code. You want to keep the scope
of your variable as narrow as possible. 

    var x int // package level scope

    func main() {
        var a int // func level scope
        fmt.Println(a)
        {  // socoped withing a code block {}
            b:= 42 
            fmt.Println(b)	
        } 
        // fmt.Println(b) // b not accessible here
    } 

### Closure incrementor

    func main() {
        a := incrementor()
        fmt.Println(a())
        fmt.Println(a())
        fmt.Println(a())
        b := incrementor()
        fmt.Println(b())
    } 
    
    // this func is enclosing its closure 
    // around x
    func incrementor() func() int {
        var x int 
        return func() int {
            x++
            return x
        }
<<<<<<< HEAD
        return f(yi...)
    }

## Recursion 

    func main() {
        fmt.Println(4*3*2*1)
        n := factorial(4)
        fmt.Println(n)
    }

    func factorial(n int) int {
        if n == 0 {
            return 1
        }
        return n * factorial(n-1)
    }

    func facLoop(n int) int {
        total := 1
        for i := n; i > 0; i-- {
            total *= i
        }
        return total
    }

## Pointer

Pointer is pointing to some location in memory where a value is stored.  Example a post office with bunch of PO boxes.
PO boxes have address and you can store value in those PO boxes. 

    func main() {
        a := 42
        fmt.Println(a)
        fmt.Println(&a) // addr. in mem. where the value of a is store

        fmt.Printf("%T\n", a) // type of a
        fmt.Printf("%T\n", &a) // type of address , *int

        // de-referencing the addr and get the value
        b := &a;
        fmt.Println(b)
        fmt.Println(*b) // get the value of what is sotred in addr of b 
        fmt.Println(*&a) // get the value of what is sotred in addr of a

        // store 43 in the address of b which is pointing to the address of where the value 
        // of a is sotred
        *b = 43
        fmt.Println(a)
    }

### When to use pointers

If you have a large chunk of data and you don't want to pass that data around throughout the program. You can just pass that address where the data is stored. 

You need to change something that is stored at a certain location, you could use pointers. 

    func main()  {
        x := 0
        foo(x)
        fmt.Println(x)

        fmt.Println("--- Pointer ---")
        // ptr
        a := 0
        bar(&a) // pass the address of a (0xc00008a018)
        fmt.Println(a)
    }

    func foo(y int) {
        fmt.Println(y)
        y = 43
        fmt.Println(y)
    }

    func bar(b *int) { // pass the location
        fmt.Println(b) // addr
        fmt.Println(*b) // get the value sotred in provided address (0xc00008a018) => 0
        *b = 43 // the value stored in this addr. set to 43
        fmt.Println(b) // addr
        fmt.Println(*b) // value
    }

## Method sets

Methods attached to TYPE are known as its method set. 

Method sets determine what methods are attached to a TYPE. It is exactly as what it says. It is the set of methods for a given type?

* a NON-POINTER RECEIVER
 * works with values that are POINTERS or NON-POINTERS
* a POINTER RECEIVER
 * only works with values that are POINTERS

## JSON Marshal, Unmarshal

### Unmarshal

    type person struct{
        First string
        Last string
        Age int
    }

    func main()  {
        s := `[{"First":"James","Last":"Bond","Age":32},{"First":"Miss","Last":"Moneypenny","Age":27}]`
        bs := []byte(s)
        fmt.Printf("%T\n", s)
        fmt.Printf("%T\n", bs)

        // people := []person{}
        var people []person

        err := json.Unmarshal(bs, &people)
        if err != nil {
            fmt.Println(err)
        }
        fmt.Println(people)

        for i, v := range people {
            fmt.Println("\nPERSON NUBMER", i)
            fmt.Println(v.First, v.Last, v.Age)
        }
    }

### Marshal

    type person struct {
        First string
        Last string
        Age int
    }

    func main()  {
        p1 := person{
            First: "James",
            Last: "Bond",
            Age: 32,
        }

        p2 := person {
            First: "Miss",
            Last: "Moneypenny",
            Age: 27,
        }

        people := []person{p1, p2}
        fmt.Println(people)

        bs, err := json.Marshal(people)
        if err != nil {
            fmt.Println(err)
        }
        fmt.Println(string(bs))
    }

### JSON Encode 

https://godoc.org/encoding/json#Decoder

Doing JSON operation straight-to or straight from the wire. If something is going out you could encode it and send it as json.
For something coming in our program decode is used to transform incoming json to GO data structure. 

    func NewEncoder(w io.Writer) *Encoder

    w is the writer Passed, to which the encoder should write its encoded output 

## Writer interface

    type Writer interface {
        Write(p []bytes) (n int, err error)
    }

    Writer is an interface. Any other type that has this method Write attached to it will also be of TYPE Writer. 

## Sort

Standard library

    func main()  {
        xi := []int{3,6,2,5,1,6,0,11,52}
        xs := []string{"a","uu","xx","ww","saw","rte"}
        
        fmt.Println(xi)
        sort.Ints(xi)
        fmt.Println(xi)

        fmt.Println("---")
        fmt.Println(xs)
        sort.Strings(xs)
        fmt.Println(xs)
    }

Custom sort

    import (
        "fmt"
        "sort"
    )

    type Person struct {
        Name string
        Age int
    }

    func (p Person) String() string {
        return fmt.Sprintf("%s: %d", p.Name, p.Age)
    }

    // ByAge implements sort.Iterface for []Person based on the Age field
    // slice of person
    type ByAge []Person

    func (a ByAge) Len() int           { return len(a) }
    func (a ByAge) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
    func (a ByAge) Less(i, j int) bool { return a[i].Age < a[j].Age }


    func main()  {
        people := []Person{
            {"Bob", 31},
            {"Ram", 11},
            {"Tom", 41},
            {"Sam", 37},
        }

        fmt.Println(people)
        sort.Sort(ByAge(people))
        fmt.Println(people)
    }


## Bcrypt

    Used for password encoding

    func main()  {
        s := "password1234"
        bs, err  := bcrypt.GenerateFromPassword([]byte(s), bcrypt.MinCost)
        if err != nil {
            fmt.Println(err)
        }

        fmt.Println(s)
        fmt.Println(string(bs))

        loginPwd := "password123"
        err = bcrypt.CompareHashAndPassword(bs, []byte(loginPwd))
        if err != nil {
            fmt.Println("You can't login")
            return
        }
        fmt.Println("Logged in")
    }

## Concurrency 

Concurrency vs Parallelism.
Go lang came in the year 2006 after intel announced it s dual-core processors. This language is made to take advantage of multiple CPUs. 

> If you write code in go on a machine that has one CPU  your code is not going to run in parallel. It is going to run sequentially. 

There won't be multiple thread of code running in parallel. 

If you have more than one CPU, then parallelism is possible. 

If you have multiple cores then code can run in parallel.

> Concurrency is a design patten. This is the way that you write your code. You can write code which has the possibility to run in parallel. If you have multiple code that code can run in parallel. 
> https://www.youtube.com/watch?v=cN_DpYBzKso


### runtime package

    const GOARCH string =  sys.GOARCH
    const GOOS string =  sys.GOOS

=======
    }

## Recursion

Recursion is when a function calls itself.
>>>>>>> 08168be96d4dd952b94b3864b7cfe75ada3ffc82
